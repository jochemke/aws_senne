#!/bin/bash

# Variables
EC2_IP="${ec2_ip}"
APP_ENDPOINT="http://${ec2_ip}/health" # or whatever your health check URL is

# Test EC2 Reachability
ping -c 3 $EC2_IP
if [ $? -ne 0 ]; then
    echo "EC2 instance is not reachable!"
    exit 1
fi

# Test Application Endpoint
curl -f $APP_ENDPOINT
if [ $? -ne 0 ]; then
    echo "Application health check failed!"
    exit 1
fi

# If you're using a database, add tests here to check its connectivity.

echo "All tests passed!"
