resource "aws_instance" "senne_vm" {
  ami           = var.ami //Ubuntu AMI
  instance_type = var.instance_type
              
  tags = {
    Name = var.name_tag
  }
}

data "template_file" "healthcheck" {
  template = file("${path.module}/healthcheck.tpl")

  vars = {
    ec2_ip = aws_instance.senne_vm.public_ip
  }
}
