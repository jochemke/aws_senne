variable "ami" {
  type        = string
  description = "Ubuntu AMI ID in us-east-1 Region"
  default     = "ami-053b0d53c279acc90"//0261755bbcb8c4a84
}

variable "instance_type" {
  type        = string
  description = "Instance type"
  default     = "t2.micro"
}

variable "name_tag" {
  type        = string
  description = "AWS-Senne instance"
  default     = "AWS-Senne"
}
